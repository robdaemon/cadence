[![Build Status](https://travis-ci.org/robertrolandorg/cadence.png)](https://travis-ci.org/robertrolandorg/cadence)

# cadence

A way to define an HBase schema and a simple mapping from a hash to / from rows
within an HBase table.

## Usage

Include the Maven dependency in your project:

    <dependency>
    	<groupId>com.github.robertrolandorg.cadence</groupId>
    	<artifactId>cadence</artifactId>
    	<version>1.0-SNAPSHOT</version>
	</dependency>

### Schemas

Schemas can either be a Map (easily represented as JSON) or POJOs.

Sample JSON Schema:

	{
	    "table": "facebook_post",
	    "delimiter": "|",
	    "key": {
	        "parts": [
	            {
	                "name": "facebook_id",
	                "type": "SHA1"
	            }           
	        ]
	    },
	    "columns": [
	        {
	            "name": "facebook_id",
	            "type": "String",
	            "family": "post"
	        },
	        {
	            "name": "created_at",
	            "type": "DateTime",
	            "family": "post"
	        },
	        {
	            "name": "title",
	            "type": "String",
	            "family": "post"
	        },
	        {
	            "name": "counts",
	            "family": "post",
	            "type": "Hash",
	            "members": [
	                {
	                    "name": "shares",
	                    "type": "Long"
	                },
	                {
	                    "name": "likes",
	                    "type": "Long"
	                },
	                {
	                    "name": "comments",
	                    "type": "Long"
	                }
	            ]
	        }
	    ]
	}

Use your favorite JSON parser to make a Map out of this (may I suggest Jackson?), then you can use this schema as:

    Map mapSchema = ...;
    SchemaAdapter adapter = new MapSchemaAdapter();
    Schema resultingSchema = adapter.translateSchema(mapSchema);

There's a lot left to be added to this usage doc, BTW.

## Notes

This uses Java 6 as a source and target, since Hadoop / HBase aren't certified to run
properly under Java 7. As soon as Cloudera certifies CDH under Java 7, I will switch
the source/target to 1.7.

This uses the Cloudera CDH3 version of HBase, only because this is the distribution
of HBase I work with on a daily basis.

## License

Copyright © 2013 Robert Roland

Distributed under the MIT License. See LICENSE.
