package org.robertroland.cadence.types

import org.apache.hadoop.hbase.util.Bytes
import org.junit.Test

import java.text.SimpleDateFormat

/**
 * 
 * @author rob
 * @since 4/2/13
 */
class DateSerializerTest extends GroovyTestCase {
    def serializer = new DateSerializer()

    @Test
    void testDeserialize() {
        def dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
        def stringRepresentation = "2013-03-10T23:41:00-0700"
        def date = dateFormat.parse(stringRepresentation)

        assertEquals date, serializer.deserialize(Bytes.toBytes(stringRepresentation))

    }

    @Test
    void testSerialize() {
        def dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
        def stringRepresentation = "2013-03-07T23:41:00-0800"
        def date = dateFormat.parse(stringRepresentation)

        assertEquals date, serializer.deserialize(serializer.serialize(date))
    }
}
