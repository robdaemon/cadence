package org.robertroland.cadence.schema

import org.junit.Test

/**
 *
 * @author rob
 * @since 3/26/13
 */
class MapSchemaAdapterImplTest extends GroovyTestCase {
    @Test
    void testSchemaMapping() {
        def sourceSchema = mapSchema()

        def impl = new MapSchemaAdapterImpl()

        def result = impl.translateSchema(sourceSchema)

        assertNotNull result

        assertEquals "|", result.getDelimiter()
        assertEquals "facebook_post", result.getTable()
        assertEquals "facebook_id", result.getCompositeKey().get(0).getColumnName()

        def columns = result.columns

        assertEquals 10, columns.size

        assertEquals "facebook_id", columns[0].columnName
        assertEquals "String", columns[0].typeName
        assertEquals "post", columns[0].columnFamily

        assertEquals "hyperlink", columns[4].formatter

        assertEquals "List", columns[6].typeName
        assertEquals "Map", columns[6].listElementType
        assertEquals 3, columns[6].mapFields().size()

        assertEquals "Map", columns[7].typeName
        assertEquals 3, columns[7].mapFields().size()

        assertEquals "Map", columns[9].typeName
        assertEquals 1, columns[9].mapFields().size()

        assertEquals "Map", columns[9].mapFields()[0].typeName
        assertEquals 2, columns[9].mapFields()[0].mapFields().size()

        assertEquals "metadata", columns[9].mapFields()[0].mapFields()[0].columnFamily
    }

    Map mapSchema() {
        return [
                table: "facebook_post",
                delimiter: "|",
                key: [
                        parts: [
                                [
                                        name: "facebook_id",
                                        type: "SHA1"
                                ]
                        ]
                ],
                columns: [
                        [
                                name: "facebook_id",
                                type: "String",
                                family: "post"
                        ],
                        [
                                name: "created_at",
                                type: "DateTime",
                                family: "post"
                        ],
                        [
                                name: "title",
                                type: "String",
                                family: "post"
                        ],
                        [
                                name: "text",
                                type: "String",
                                family: "post"
                        ],
                        [
                                name: "link",
                                type: "String",
                                family: "post",
                                formatter: "hyperlink"
                        ],
                        [
                                name: "privacy",
                                type: "String",
                                family: "post"
                        ],
                        [
                                name: "to",
                                type: "VectorHash",
                                family: "post",
                                members: [
                                        [
                                                name: "name",
                                                type: "String"
                                        ],
                                        [
                                                name: "id",
                                                type: "String"
                                        ],
                                        [
                                                name: "link",
                                                type: "String",
                                                formatter: "hyperlink"
                                        ]
                                ]
                        ],
                        [
                                name: "user",
                                type: "Hash",
                                family: "post",
                                members: [
                                        [
                                                name: "name",
                                                type: "String"
                                        ],
                                        [
                                                name: "id",
                                                type: "String"
                                        ],
                                        [
                                                name: "link",
                                                type: "String",
                                                formatter: "hyperlink"
                                        ]
                                ]
                        ],
                        [
                                name: "counts",
                                type: "Hash",
                                family: "counts",
                                members: [
                                        [
                                                name: "shares",
                                                type: "Long"
                                        ],
                                        [
                                                name: "comments",
                                                type: "Long"
                                        ],
                                        [
                                                name: "likes",
                                                type: "Long"
                                        ]
                                ]
                        ],
                        [
                                name: "metadata",
                                type: "Hash",
                                family: "metadata",
                                members: [
                                        [
                                                name: "accounts",
                                                type: "Hash",
                                                members: [
                                                        [
                                                                name: "account",
                                                                type: "String"
                                                        ],
                                                        [
                                                                name: "tags",
                                                                type: "List",
                                                                value_type: "String"
                                                        ]
                                                ]
                                        ]
                                ]
                        ]
                ]
        ]
    }
}
