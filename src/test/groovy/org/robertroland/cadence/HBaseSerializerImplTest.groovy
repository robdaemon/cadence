/*
 * Copyright (c) 2013 Robert Roland
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package org.robertroland.cadence

import org.apache.hadoop.hbase.util.Bytes
import org.junit.Test
import org.robertroland.cadence.schema.TestSchemas

import static org.junit.Assert.*

/**
 * 
 * @author robert@robertroland.org
 * @since 3/19/13
 */
class HBaseSerializerImplTest extends GroovyTestCase {
    @Test
    void testBuildRowKey() {
        def schema = TestSchemas.compositeKeySchema()

        def serializer = new HBaseSerializerImpl(schema)

        def entity = [
                main_id: "foo12345",
                timestamp: 1363677363L
        ]

        def result = serializer.buildRowKey(entity)

        assertArrayEquals Bytes.toBytes("f170550f744f4b6ffb190a742223c05ac8031ed9|1363677363"), result
    }

    @Test
    void testSerialize() {
        def schema = TestSchemas.compositeKeySchema()

        def serializer = new HBaseSerializerImpl(schema)

        def entity = [
                main_id: "foo12345",
                timestamp: 1363677363L,
                testColumn1: 1234,
                testColumn2: "TestString12345",
                stringList: [
                        "Test1",
                        "Test2",
                        "Test3"
                ],
                metadata: [
                        customers: [
                                [
                                        accountId: "account1",
                                        tags: [
                                                "account1tag1",
                                                "account1tag2",
                                                "account1tag3"
                                        ]
                                ],
                                [
                                        accountId: "account2",
                                        tags: [
                                                "account2tag1"
                                        ]
                                ]
                        ]
                ]
        ]

        def result = serializer.serializeObject(entity)

        assertArrayEquals Bytes.toBytes("f170550f744f4b6ffb190a742223c05ac8031ed9|1363677363"), result[0].getRow()
    }
}
