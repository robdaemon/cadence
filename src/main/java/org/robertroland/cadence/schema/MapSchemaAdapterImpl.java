/*
 * Copyright (c) 2013 Robert Roland
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package org.robertroland.cadence.schema;

import org.apache.commons.lang.StringUtils;
import org.robertroland.cadence.model.Field;
import org.robertroland.cadence.model.Schema;

import java.util.List;
import java.util.Map;

/**
 * Translate a cadence schema from a Map to a full cadence schema
 *
 * @author robert@robertroland.org
 * @since 3/10/13
 */
public class MapSchemaAdapterImpl implements SchemaAdapter<Map> {
    public static final String TABLE = "table";
    public static final String DELIMITER = "delimiter";
    public static final String NAME = "name";
    public static final String FAMILY = "family";
    public static final String TYPE = "type";
    public static final String FORMATTER = "formatter";
    public static final String PARTS = "parts";
    public static final String KEY = "key";
    public static final String COLUMNS = "columns";
    public static final String HASH = "Hash";
    public static final String MAP = "Map";
    public static final String MEMBERS = "members";
    public static final String VECTOR_HASH = "VectorHash";
    public static final String LIST = "List";
    public static final String VALUE_TYPE = "value_type";

    @Override
    public Schema translateSchema(Map sourceSchema) {
        Schema result = new Schema();

        result.setTable((String)sourceSchema.get(TABLE));
        result.setDelimiter((String)sourceSchema.get(DELIMITER));

        Map keyMap = (Map)sourceSchema.get(KEY);
        List<Map> keyParts = (List<Map>)keyMap.get(PARTS);

        for(Map part: keyParts) {
            result.getCompositeKey().add(processField(part, false));
        }

        List<Map> columns = (List<Map>)sourceSchema.get(COLUMNS);
        for(Map column: columns) {
            result.getColumns().add(processField(column, true));
        }

        return result;
    }

    protected Field processField(Map field, boolean columnFamilyRequired) {
        return processField(field, null, columnFamilyRequired);
    }

    protected Field processField(Map field, Field parentField, boolean columnFamilyRequired) {
        Field result = new Field();

        result.setColumnName((String)field.get(NAME));
        result.setFormatter((String) field.get(FORMATTER));

        if(field.containsKey(FAMILY)) {
            result.setColumnFamily((String)field.get(FAMILY));
        } else if(parentField != null) {
            result.setColumnFamily(parentField.getColumnFamily());
            result.setParent(parentField);
        }

        if(columnFamilyRequired && StringUtils.isBlank(result.getColumnFamily())) {
            throw new SchemaValidationException("Column family is required for " + field);
        }

        String type = (String)field.get(TYPE);

        if(HASH.equals(type)) {
            result.setTypeName(MAP);

            List<Map> children = (List<Map>)field.get(MEMBERS);

            processMapFieldChildren(result, children);
        } else if(VECTOR_HASH.equals(type)) {
            result.setTypeName(LIST);
            result.setListElementType(MAP);

            List<Map> children = (List<Map>)field.get(MEMBERS);

            processMapFieldChildren(result, children);
        } else if(LIST.equals(type)) {
            result.setTypeName(LIST);
            result.setListElementType((String)field.get(VALUE_TYPE));
        } else {
            result.setTypeName((String)field.get(TYPE));
        }

        return result;
    }

    private void processMapFieldChildren(Field result, List<Map> children) {
        for(Map child: children) {
            Field mapField = processField(child, result, true);

            result.addMapField(mapField);
        }
    }
}
