/*
 * Copyright (c) 2013 Robert Roland
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package org.robertroland.cadence;

import com.google.common.collect.Maps;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.reflections.Reflections;
import org.robertroland.cadence.formatters.Formatter;
import org.robertroland.cadence.formatters.TypeFormatter;
import org.robertroland.cadence.types.Serializer;
import org.robertroland.cadence.types.TypeSerializer;

import java.util.Map;
import java.util.Set;

/**
 * @author robert@robertroland.org
 * @since 3/7/13
 */
public class FormatterFactory {
    private static final Log LOG = LogFactory.getLog(FormatterFactory.class);

    private static final Map<String, Formatter<?>> formatters = Maps.newHashMap();

    static {
        Reflections reflections = new Reflections("org.robertroland.cadence");

        Set<Class<?>> annotated = reflections.getTypesAnnotatedWith(TypeFormatter.class);

        for(Class<?> c: annotated) {
            TypeFormatter s = c.getAnnotation(TypeFormatter.class);
            String typeName = s.value();

            try {
                Object instance = c.newInstance();

                if(!(instance instanceof Formatter)) {
                    LOG.error(String.format(
                            "Formatters must implement the Formatter interface, cannot instantiate %s",
                            instance.getClass().getName()));
                } else {
                    LOG.info(String.format("Instantiated formatter %s", typeName));
                    formatters.put(typeName, (Formatter)instance);
                }
            } catch(InstantiationException ie) {
                LOG.error(String.format("Unable to instantiate formatter for %s", typeName), ie);
            } catch(IllegalAccessException iae) {
                LOG.error(String.format("Unable to instantiate formatter for %s", typeName), iae);
            }
        }
    }

    public static Formatter<?> getFormatter(String name) {
        return formatters.get(name);
    }
}
