package org.robertroland.cadence.types;

import org.apache.hadoop.hbase.util.Bytes;
import org.robertroland.cadence.utils.ISO8601DateFormatter;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;

/**
 * @author rob
 * @since 4/2/13
 */
@TypeSerializer(value = "Date")
public class DateSerializer implements Serializer<Date> {
    private ThreadLocal<DateFormat> dateFormatISO = new ISO8601DateFormatter();

    @Override
    public Date deserialize(byte[] bytes) {
        try {
            return dateFormatISO.get().parse(Bytes.toString(bytes));
        } catch(ParseException pe) {
            throw new TypeDeserializerException("Unable to parse date", pe, bytes);
        }
    }

    @Override
    public byte[] serialize(Date object) {
        return Bytes.toBytes(dateFormatISO.get().format(object));
    }
}
