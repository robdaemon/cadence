/*
 * Copyright (c) 2013 Robert Roland
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package org.robertroland.cadence.types;

import org.apache.hadoop.hbase.util.Bytes;
import org.robertroland.cadence.utils.ISO8601DateFormatter;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Serializes a Date as an ISO-8601 string.
 *
 * @author robert@robertroland.org
 * @since 3/7/13
 */
@TypeSerializer(value = "DateTime")
public class ISO8601DateTimeSerializer implements Serializer<String> {
    private ThreadLocal<DateFormat> dateFormatISO = new ISO8601DateFormatter();

    @Override
    public String deserialize(byte[] bytes) {
        return Bytes.toString(bytes);
    }

    @Override
    public byte[] serialize(String object) {
        try {
            dateFormatISO.get().parse(object);
        } catch(ParseException pe) {
            throw new TypeSerializerException("Unable to parse date", pe, object);
        }

        return Bytes.toBytes(object);
    }
}
