package org.robertroland.cadence.types;

import org.codehaus.jackson.map.ObjectMapper;

import java.io.ByteArrayInputStream;

/**
 * Adds the ability to serialize or deserialize anything that Jackson can parse as JSON.
 *
 * @author robert@robertroland.org
 * @since 3/30/13
 */
@TypeSerializer("JSON")
public class JSONSerializer implements Serializer<Object> {
    private ObjectMapper objectMapper;

    public JSONSerializer() {
        objectMapper = new ObjectMapper();
    }

    @Override
    public Object deserialize(byte[] bytes) {
        try {
            return objectMapper.readValue(new ByteArrayInputStream(bytes), Object.class);
        } catch(Exception ex) {
            throw new TypeDeserializerException("Unable to parse JSON", ex, bytes);
        }
    }

    @Override
    public byte[] serialize(Object object) {
        try {
            return objectMapper.writeValueAsBytes(object);
        } catch(Exception ex) {
            throw new TypeSerializerException("Unable to generate JSON for given object", ex, object);
        }
    }
}
