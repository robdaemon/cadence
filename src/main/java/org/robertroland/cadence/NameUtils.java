/*
 * Copyright (c) 2013 Robert Roland
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package org.robertroland.cadence;

import org.robertroland.cadence.model.Field;
import org.robertroland.cadence.model.PositionField;
import org.robertroland.cadence.model.Schema;

import java.util.Deque;

/**
 * @author robert@robertroland.org
 * @since 4/13/13
 */
public class NameUtils {
    public static String generateName(Deque<Field> fieldDeque, Schema schema, String nameSuffix) {
        StringBuilder sb = new StringBuilder();

        for(Field field: fieldDeque) {
            if(sb.length() > 0) {
                sb.insert(0, schema.getDelimiter());
            }

            if(field instanceof PositionField) {
                sb.insert(0, ((PositionField)field).getPositionString());
            } else {
                sb.insert(0, field.getColumnName());
            }
        }

        if(nameSuffix != null) {
            sb.append(schema.getDelimiter()).append(nameSuffix);
        }

        return sb.toString();
    }
}
