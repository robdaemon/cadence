package org.robertroland.cadence.formatters;

/**
 *
 *
 * @author robert@robertroland.org
 * @since 3/23/13
 */
public interface Formatter<T> {
    public String format(T object);
}
