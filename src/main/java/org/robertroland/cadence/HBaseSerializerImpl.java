/*
 * Copyright (c) 2013 Robert Roland
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package org.robertroland.cadence;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Row;
import org.apache.hadoop.hbase.util.Bytes;
import org.robertroland.cadence.model.Field;
import org.robertroland.cadence.model.PositionField;
import org.robertroland.cadence.model.Schema;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.*;

/**
 * @author robert@robertroland.org
 * @since 3/18/13
 */
public class HBaseSerializerImpl implements Serializer<List<Row>> {
    private static final Log LOG = LogFactory.getLog(HBaseSerializerImpl.class);

    private Schema schema;

    public HBaseSerializerImpl(Schema schema) {
        this.schema = schema;
    }

    @Override
    public List<Row> serializeObject(Map<String, Object> entity) {
        List<Row> results = Lists.newLinkedList();

        Put put = new Put(buildRowKey(entity));

        new StatefulSerializer(entity, schema, put).handleSchema();

        results.add(put);

        return results;
    }

    @Override
    public byte[] buildRowKey(Map<String, Object> entity) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        List<Field> keySchema = schema.getCompositeKey();

        boolean first = true;

        try {
            for(Field field: keySchema) {
                if(!first) {
                    baos.write(Bytes.toBytes(schema.getDelimiter()));
                } else {
                    first = false;
                }

                Object value = entity.get(field.getColumnName());

                baos.write(serializeValue(value, field));
            }
        } catch(IOException ioe) {
            LOG.error("Unable to serialize rowkey", ioe);

            return null;
        }

        return baos.toByteArray();
    }

    private byte[] serializeValue(Object value, Field field) {
        final String type;
        if("List".equals(field.getTypeName())) {
            type = field.getListElementType();
        } else {
            type = field.getTypeName();
        }

        org.robertroland.cadence.types.Serializer serializer = null;

        if(field.getFormatter() != null) {
            org.robertroland.cadence.formatters.Formatter formatter =
                    FormatterFactory.getFormatter(field.getFormatter());

            value = formatter.format(value);

            serializer = TypeSerializerFactory.getSerializer("String");
        } else {
            serializer = TypeSerializerFactory.getSerializer(type);
        }

        if(value != null) {
            return serializer.serialize(value);
        } else {
            return null;
        }
    }

    protected class StatefulSerializer {
        private Map<String, Object> entity;
        private Schema schema;
        private Put put;

        private Map<String, byte[]> cachedFamilies = Maps.newHashMap();

        private Deque<Field> fieldDeque = new ArrayDeque<Field>();

        public StatefulSerializer(Map<String, Object> entity, Schema schema, Put put) {
            this.entity = entity;
            this.schema = schema;
            this.put = put;
        }

        public void handleSchema() {
            for(Field field: schema.getColumns()) {
                handleField(field, null);
            }
        }

        protected void handleField(Field field, String nameSuffix) {
            fieldDeque.push(field);

            String fieldTypeName = field.getTypeName();

            if("Map".equals(fieldTypeName)) {
                for(Field mapField: field.mapFields()) {
                    handleField(mapField, null);
                }
            } else if("List".equals(fieldTypeName)) {
                String name = NameUtils.generateName(fieldDeque, schema, null);

                List list = (List)getValue(name);

                // serialize the size of the list
                put.add(getCachedColumnFamily(field),
                        Bytes.toBytes(String.format("%s%s_count", name, schema.getDelimiter())),
                        Bytes.toBytes((long)list.size()));

                boolean map = "Map".equals(field.getListElementType());

                for(long l = 0; l < list.size(); l++) {
                    PositionField positionField = new PositionField(l, field);

                    fieldDeque.push(positionField);

                    if(map) {
                        for(Field mapField: field.mapFields()) {
                            handleField(mapField, null);
                        }
                    } else {
                        handleValue(field, field.getListElementType(), null);
                    }

                    fieldDeque.pop();
                }
            } else {
                handleValue(field, null, nameSuffix);
            }

            fieldDeque.pop();
        }

        protected void handleValue(Field field, String typeOverride, String nameSuffix) {
            byte[] family = getCachedColumnFamily(field);

            String name = NameUtils.generateName(fieldDeque, schema, nameSuffix);

            Object value = getValue(name);

            put.add(family, Bytes.toBytes(name), serializeValue(value, field));
        }

        protected Object getValue(String name) {
            String[] parts = StringUtils.split(name, schema.getDelimiter());

            Object result = entity;

            for(String part: parts) {
                if(result instanceof Map) {
                    result = ((Map) result).get(part);
                } else if(result instanceof List) {
                    result = ((List) result).get(Integer.parseInt(part));
                } else {
                    throw new IllegalArgumentException("unable to traverse this object type");
                }
            }

            return result;
        }

        private byte[] getCachedColumnFamily(Field field) {
            String columnFamilyString = field.getColumnFamily();
            byte[] family;
            if(!cachedFamilies.containsKey(columnFamilyString)) {
                cachedFamilies.put(columnFamilyString, Bytes.toBytes(columnFamilyString));
            }

            family = cachedFamilies.get(columnFamilyString);
            return family;
        }
    }
}
