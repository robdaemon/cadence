package org.robertroland.cadence;

import java.util.Map;

/**
 * @author robert@robertroland.org
 * @since 3/18/13
 */
public interface Serializer<T> {
    /**
     * Prepare an object to be persisted
     * @param entity to be persisted
     * @return
     */
    public T serializeObject(Map<String, Object> entity);

    public byte[] buildRowKey(Map<String, Object> entity);
}
