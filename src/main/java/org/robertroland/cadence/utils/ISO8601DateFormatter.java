package org.robertroland.cadence.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

/**
 * @author rob
 * @since 4/2/13
 */
public class ISO8601DateFormatter extends ThreadLocal<DateFormat> {
    @Override
    protected DateFormat initialValue() {
        return new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
    }
}
