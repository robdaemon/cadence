/*
 * Copyright (c) 2013 Robert Roland
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package org.robertroland.cadence;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.hadoop.hbase.KeyValue;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.util.Bytes;
import org.robertroland.cadence.model.Field;
import org.robertroland.cadence.model.PositionField;
import org.robertroland.cadence.model.RowKey;
import org.robertroland.cadence.model.Schema;
import org.robertroland.cadence.types.Serializer;

import java.util.*;

/**
 * A Deserializer instance that uses the HBase Client API
 *
 * @author robert@robertroland.org
 * @since 2/24/13
 */
public class HBaseDeserializerImpl implements Deserializer<Result> {
    private Schema schema;

    public HBaseDeserializerImpl(Schema schema) {
        this.schema = schema;
    }

    @Override
    public Map<Object, Object> deserialize(Result databaseResult) {
        if(databaseResult == null) {
            return null;
        }

        Map<Object, Object> result = new HashMap<Object, Object>();

        result.put("__rowkey", new RowKey(databaseResult.getRow()));

        new StatefulDeserializer(schema, databaseResult, result).deserialize();

        return result;
    }

    protected class StatefulDeserializer {
        private Schema schema;
        private Result dbResult;
        private Map<String, byte[]> cachedFamilies = Maps.newHashMap();
        private Deque<Field> fieldDeque = new ArrayDeque<Field>();

        private Map<Object, Object> result;

        public StatefulDeserializer(Schema schema, Result dbResult, Map<Object, Object> result) {
            this.schema = schema;
            this.dbResult = dbResult;
            this.result = result;
        }

        public void deserialize() {
            for(Field field: schema.getColumns()) {
                result.put(field.getColumnName(), handleResult(field, null));
            }
        }

        protected Object handleResult(Field field, String typeOverride) {
            fieldDeque.push(field);

            Object result = null;
            final String type = typeOverride != null ? typeOverride : field.getTypeName();

            if("Map".equals(type)) {
                result = deserializeMap(field);
            } else if("List".equals(type)) {
                result = deserializeList(field);
            } else {
                Serializer serializer = TypeSerializerFactory.getSerializer(type);

                String columnName = NameUtils.generateName(fieldDeque, schema, null);

                KeyValue kv = dbResult.getColumnLatest(getCachedColumnFamily(field),
                        Bytes.toBytes(columnName));

                if(kv != null) {
                    result = serializer.deserialize(kv.getValue());
                }
            }

            fieldDeque.pop();

            return result;
        }

        protected Map<Object, Object> deserializeMap(Field field) {
            Map<Object, Object> result = Maps.newHashMap();

            for(Field mapField: field.mapFields()) {
                result.put(mapField.getColumnName(), handleResult(mapField, null));
            }

            return result;
        }

        protected List<Object> deserializeList(Field field) {
            List<Object> result = Lists.newArrayList();

            String name = NameUtils.generateName(fieldDeque, schema, null);

            byte[] sizeColumn = dbResult.getValue(getCachedColumnFamily(field), Bytes.toBytes(
                    String.format("%s%s_count", name, schema.getDelimiter())));

            if(sizeColumn == null) {
                return null;
            }

            long size = Bytes.toLong(sizeColumn);

            for(long l = 0; l < size; l++) {
                PositionField positionField = new PositionField(l, field);

                result.add(handleResult(positionField, field.getListElementType()));
            }

            return result;
        }

        protected byte[] getCachedColumnFamily(Field field) {
            String columnFamilyString = field.getColumnFamily();
            byte[] family;
            if(!cachedFamilies.containsKey(columnFamilyString)) {
                cachedFamilies.put(columnFamilyString, Bytes.toBytes(columnFamilyString));
            }

            family = cachedFamilies.get(columnFamilyString);
            return family;
        }
    }
}
